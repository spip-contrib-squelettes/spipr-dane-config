<?php
/**
 * Fonctions utiles au plugin Spipr-Dane Config
 *
 * @plugin     Spipr-Dane Config
 * @copyright  2019
 * @author     Webmestre DANE
 * @licence    GNU/GPL
 * @package    SPIP\Sdc\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
