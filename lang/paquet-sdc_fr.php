<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sdc_description' => 'Permet de modifier les couleurs, arrière pla et police du squelette SPIPr-Dane ',
	'sdc_nom' => 'SPIPr-Dane Config',
	'sdc_slogan' => 'Personnalisation graphique du squelette Spipr-Dane',
);
